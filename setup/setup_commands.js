let setupCommands = (ostype, ostypes, config) => {

    const mac_arm_cmds = {
        download_kubectl: `curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/darwin/arm64/kubectl"`,
        install_kubectl: `chmod +x ./kubectl && sudo mv ./kubectl /usr/local/bin/kubectl && sudo chown root: /usr/local/bin/kubectl`,
        setup_kubectl: `curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-darwin-arm64 && sudo install minikube-darwin-arm64 /usr/local/bin/minikube`,
    };

    const mac_x86_cmds = {
        download_kubectl: `curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/darwin/amd64/kubectl"`,
        install_kubectl: `chmod +x ./kubectl && sudo mv ./kubectl /usr/local/bin/kubectl && sudo chown root: /usr/local/bin/kubectl`,
        setup_kubectl: `curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-darwin-amd64 && sudo install minikube-darwin-amd64 /usr/local/bin/minikube`,
    };

    const linux_cmds = {
        download_kubectl: `curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"`,
        install_kubectl: `sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl`,
        setup_kubectl: `curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && sudo install minikube-linux-amd64 /usr/local/bin/minikube`,
    };

    const common_cmds = {
        start_minikube: `minikube start --memory ${config.minikube_memory}`,
        install_helm: `curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 && chmod 700 get_helm.sh && ./get_helm.sh`,
        update_helm_chart: `helm repo add hashicorp https://helm.releases.hashicorp.com`,
        install_consul: `helm install --values ${config.helm_config} consul hashicorp/consul --create-namespace --namespace ${config.namespace} --version "${config.consul_version}"`,
        delete_consul: `helm uninstall consul --namespace ${config.namespace}`,
        minikube_tunnel: `minikube tunnel`
    };

    let platform_cmds = {}
    if (ostype == ostypes.mac_arm) {
        platform_cmds = mac_arm_cmds
    }
    if (ostype == ostypes.mac_x86) {

        platform_cmds = mac_x86_cmds
    }
    if (ostype == ostypes.linux) {
        platform_cmds = linux_cmds
    }

    let commands = {
        // platform specific commands
        ...platform_cmds,
        // common commands
        ...common_cmds,
    }
    return commands;
}

export default setupCommands;
