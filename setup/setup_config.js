let config = {
    // show debug logs for each setup command
    debug: true,
    // memory used by minikube vm
    minikube_memory: '4096',
    // for releases and versions, visit to https://github.com/hashicorp/consul-k8s/tags
    consul_version: '0.47.1',
    // kubernetes namespace where consul will be deployed
    namespace: 'consul',
    consului_port: '8080',
    helm_config: 'helm-consul-values.yaml',
}

export default config;