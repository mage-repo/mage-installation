import chalk from 'chalk';
import shell from 'shelljs';
import setupCommands from "./setup_commands.js";
import config from './setup_config.js';


let silent = !config.debug
let ostype = ""
let ostypes = {
    mac_x86: "mac_x86",
    mac_arm: "mac_arm",
    mac: "mac",
    linux: "linux",
}

let setOS = (osTypeInput)=> {
    let osdict = {
        "darwin": ostypes.mac,
        "linux": ostypes.linux,
      };

    Object.entries(osdict).forEach(([k,v]) => {
        if (osTypeInput.toLowerCase().includes(k.toLowerCase())) {
            ostype = v
            if (ostype == ostypes.mac) {
                setOSMac();
            }
            console.log(`${ostype} detected for installation`)
        }
    })
    if (ostype == "") {
        throw new Error(`unknown os type ${osTypeInput}`)
    }
}

function setOSMac() {
    let macArch = shell.exec("uname -m", {silent}).stdout
    if (macArch.trim() == "arm64") {
        ostype = ostypes.mac_arm
    }
    if (macArch.trim() == "x86_64") {
        ostype = ostypes.mac_x86
    }
}

let executeCmd = (cmd , errString = "") => {
    if ( !silent) {
        console.log(`RUN: ${cmd}`)
    }
    let res = shell.exec(cmd, {silent})
    if ( res.code != 0 ) {
            throw new Error(`${cmd}:${errString}\n`+ chalk.red(`${res.stderr}`))
    }
}

async function setupMesh() {

    // detect host os
    let res = shell.exec("uname -s",{silent})
    setOS(res.stdout)

    // get commands
    let commands = setupCommands(ostype, ostypes, config)

    //setup kubectl
    if ( shell.exec("which kubectl", {silent}).code == 0) {
        console.log("kubectl already installed")
    } else {
        console.log(chalk.blue("\n* Installing kubectl"))
        executeCmd(commands.download_kubectl)
        executeCmd(commands.install_kubectl)
    }

    //setup minikube
    if ( shell.exec("which minikube", {silent}).code == 0) {
        console.log("Minikube already installed")
    } else {
        console.log(chalk.blue("\n* Installing minikube"))
        executeCmd(commands.setup_minkube)
    }
    console.log(chalk.blue("\n* Starting minikube"))
    executeCmd(commands.start_minikube)

    //setup helm
    if ( shell.exec("which helm", {silent}).code == 0) {
        console.log("Helm already installed")
    } else {
        console.log(chalk.blue("\n* Installing helm"))
        executeCmd(commands.install_helm)
    }
    executeCmd(commands.update_helm_chart)

    //deploy consul
    if ( shell.exec(`helm status consul --namespace ${config.namespace}`, {silent}).code == 0) {
        console.log("Consul already deployed")
        console.log("\n* Deleting existing consul release")
        executeCmd(commands.delete_consul)
    }

    console.log(chalk.blue(`\n* Deploying consul, namespace - ${config.namespace}`))
    executeCmd(commands.install_consul)
    console.log(chalk.blue(`* Successfully deployed consul`))

    console.log(`\nTo see a list of consul pods, Run this cmd in other terminal\n`+ chalk.green(`kubectl get pods --namespace ${config.namespace}\n`))
    console.log(`\nTo see a list of consul services, Run in other terminal\n`+ chalk.green(`kubectl get svc --namespace ${config.namespace}\n`))

    //tunnel minikube to assign IP address for load balancer services
    console.log("Tunneling minikube")
    executeCmd(commands.minikube_tunnel)
}

setupMesh()