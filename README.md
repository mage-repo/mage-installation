# Mage Installation

Installer scripts and resources for starting up mage on any machine.
To install, run the following command:
```
curl -o- https://gitlab.com/mage-repo/mage-installation/-/raw/main/mageinstaller.sh | bash
```
