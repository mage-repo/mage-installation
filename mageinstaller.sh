#! /bin/bash

export NVM_DIR="$HOME/.nvm"
MAGE="mage"
MAGEUI="mage-ui"
MAGEUIBFF="mage-ui-bff"
REDIS="redis"
S3="s3-storage-service"
ORCHESTRATOR="orchestrator"
GRPC="grpc-service"
MONITORING="monitoring-service"
GENERATED="generated"
PWD=$(pwd)
SQL="sql-service"

mkdir -p ~/mage
cd ~/mage
#Installing golang
sudo apt update
if hash go
then
    echo "golang already installed"
else
    echo "Installing golang"
    wget https://go.dev/dl/go1.17.6.linux-amd64.tar.gz || { echo 'wget https://go.dev/dl/go1.17.6.linux-amd64.tar.gz failed' ; exit 1; }
    sudo tar -xvf go1.17.6.linux-amd64.tar.gz
    sudo mv go /usr/local
    
    
    #sudo apt install golang-go
    echo "Updating .bashrc file"
    cd ~
    echo "export GOROOT=/usr/local/go" >> ".bashrc"
    echo "export GOPATH=$HOME/go" >> ".bashrc"
    echo "export PATH=$GOPATH/bin:$GOROOT/bin:$PATH" >> ".bashrc"
    source ~/.bashrc
    if hash go
    then
        echo "go installation sucessful"
    else
        echo "could not install go. Install it manually" ; exit 1;
    fi

    
fi

#Intalling nvm
if hash nvm
then
    echo "nvm already installed"
else
    echo "Installing nvm"
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash || { echo 'curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh failed' ; exit 1; }
    command -v nvm
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
fi

# installing node, npm
if hash node
then
    echo "node already installed"
else
    echo "Installing node"
    nvm install --lts || { echo 'node installation failed. Install it manually' ; exit 1; }
fi

#Installing protoc
if hash protoc
then
    echo "protoc already installed"
else
    echo "Installing protoc"
    sudo apt install -y protobuf-compiler
    protoc --version
    
fi

#Installing java
if hash java
then
    echo "java already installed"
else
    echo "Installing java"
    sudo apt install openjdk-11-jdk
    java --version
    
fi


#Clone Mage-ui and mage-ui-bff

if [ -d "$MAGE" ]
then
    echo "$MAGE exists"
    cd $MAGE
else
    echo "Creating dir $MAGE"
    mkdir -p $MAGE || exit 1
    cd $MAGE
fi



if [ -d "$MAGEUI" ]
then
    echo "$MAGEUI exists"
else
    echo "Cloning mage-ui"
    echo "Enter gitlab access token when prompted for password"
    git clone https://gitlab.com/mage-io/app/mage-ui || { echo 'git clone https://gitlab.com/mage-io/app/mage-ui failed' ; cd $PWD ; exit 1; }
    echo "mage-ui cloned. installing ui now..."
    cd mage-ui
    npm install
    cd ..
fi



if [ -d "$MAGEUIBFF" ]
then
    echo "$MAGEUIBFF exists"
else
    echo "Cloning mage-ui-bff"
    echo "Enter gitlab access token when prompted for password"
    git clone https://gitlab.com/mage-io/app/mage-ui-bff  || { echo 'git clone https://gitlab.com/mage-io/app/mage-ui-bff failed' ; cd $PWD ; exit 1; }
    echo "mage-ui-bff cloned. installing mage-ui-bff..."
    cd mage-ui-bff
    npm install
    cd ..
fi



#Cloning other repositories
if [ -d "~/.bashrc" ]
then
    source ~/.bashrc
fi

if [ -d "~/.zshrc" ]
then
    source ~/.zshrc
fi

SRCPATH="$GOPATH/src/gitlab.com/mage-io"
if [ -d "$SRCPATH" ]
then
    echo "$SRCPATH exists"
    cd $SRCPATH
else
    echo "Creating dir $SRCPATH"
    mkdir -p $SRCPATH || exit 1
    cd $SRCPATH
fi


if [ -d "$REDIS" ]
then
    echo "$REDIS exists"
else
    echo "Cloning redis-service"
    echo "Enter gitlab access token when prompted for password"
    git clone https://gitlab.com/mage-io/modules/redis-service  || { echo 'https://$USERNAME@gitlab.com/mage-io/modules/redis-service failed' ; cd $PWD ; exit 1; }
    echo "redis-service cloned"
    echo "renaming redis-service to redis"
    mv redis-service redis
    echo "redis installation complete"

fi

if [ -d "$SQL" ]
then
    echo "$SQL exists"
else
    echo "Cloning sql-service"
    echo "Enter gitlab access token when prompted for password"
    git clone https://gitlab.com/mage-io/modules/sql-service  || { echo 'git clone https://gitlab.com/mage-io/modules/sql-service failed' ; cd $PWD ; exit 1; }
    echo "sql-service installation complete"
fi


if [ -d "$S3" ]
then
    echo "$S3 exists"
else
    echo "Cloning s3-storage-service"
    echo "Enter gitlab access token when prompted for password"
    git clone https://gitlab.com/mage-io/modules/s3-storage-service  || { echo 'git clone https://gitlab.com/mage-io/modules/s3-storage-service failed' ; cd $PWD ; exit 1; }
    echo "s3-storage-service installation complete"
fi

if [ -d "$ORCHESTRATOR" ]
then
    echo "$ORCHESTRATOR exists"
else
    echo "Cloning orchestrator"
    echo "Enter gitlab access token when prompted for password"
    git clone https://gitlab.com/mage-io/modules/orchestrator  || { echo 'https://gitlab.com/mage-io/orchestrator failed' ; cd $PWD ; exit 1; }
    echo "orchestrator installation complete"
fi

if [ -d "$GRPC" ]
then
    echo "$GRPC exists"
else
    echo "Cloning grpc service"
    echo "Enter gitlab access token when prompted for password"
    git clone https://gitlab.com/mage-io/modules/grpc-service  || { echo 'https://gitlab.com/mage-io/grpc-service failed' ; cd $PWD ; exit 1; }
    echo "grpc service installation complete"
fi

if [ -d "$MONITORING" ]
then
    echo "$MONITORING exists"
else
    echo "Cloning monitoring service"
    echo "Enter gitlab access token when prompted for password"
    git clone https://gitlab.com/mage-io/modules/monitoring-service  || { echo 'https://gitlab.com/mage-io/monitoring-service failed' ; cd $PWD ; exit 1; }
    echo "monitoring service installation complete"
fi


if [ -d "$GENERATED" ]
then
    echo "$GENERATED exists"
else
    echo "Cloning generated"
    echo "Enter gitlab access token when prompted for password"
    git clone https://gitlab.com/mage-io/generated  || { echo 'https://gitlab.com/mage-io/generated failed' ; cd $PWD ; exit 1; }
    echo "generated installation complete"
fi

cd $PWD

echo "Mage installation complete!"
echo "To verify, please try to run mage-ui and mage-ui-bff, and then create a new build using the UI."
echo "A successful merge request creation at the end should denote a successful installation of the application."
echo "If you are unsure how to create a new build, please ask anyone from mage team and we will happily help you"


 

